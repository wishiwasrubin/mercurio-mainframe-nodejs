# How to contribute to Mainframe
**Mainframe** is part of the **Mercurio** stack, as such it is the most basic and fundamental piece of the puzzle, since it powers the whole environment.

Mainframe is a *MVC*, *microserviced*, *12 factor* app. If you are not familiar with these terms it may sound like fuzzwords right now, but they are very important characteristics of this app. In this document we'll see how these words apply to Mainframe and will not touch how does it apply to all of Mercurio, however most of these principles are still applied to the rest of the stack.

Design guidelines:
1. [MVC](#MVC,-Model-View-Controller-design-pattern)
2. [Microservices](#Microservices-architecture)
3. [12 factor](#12-factor-app)

Contributing guidelines:
1. [General advice](#contributing-guidelines)
1. [Issues](#issuing-features)
2. [Commits](#commiting-features)

## Design guidelines

### MVC, Model-View-Controller design pattern
Or more precisely, just *MC* in this case. Mainframe contains the *Models* and the *Controllers* for Mercurio, which is completed with the addition of an independent *View* layer.

Such design allows for easy accomodations, interpretations and implementations of an user interface, which makes possible to, for instance, develop a website and a mobile app for it in record time with syncing features because they share not only the same database, but the same models to access it and the same controllers to manage the app logic.

To enhance this design, Mainframe provides a CRUD API, accessible via HTTP requests at multiple endpoints, which all serve standarised JSON responses.

As a contributor to Mainframe your code should never interfere with anything *View* for the app, *Models* and *Controllers* only. Likely, your code that produces API output should follow the standard detailed below:

```json
{
    "status": "string",
    "data": {
        "dataObject": {},
        "[optional] message": "string",
    }
}
```

E.g: 
```json
{
    "status": "success",
    "data": { 
        "user": { ... },
     }
}
```
Or:
```json
{
    "status": "error",
    "data": {
        "message": "Invalid or malformed payload."
    }
}
```

### Microservices architecture
Mainframe is composed of tiny little pieces of code, each having it's own, single, only responsability and serving it's single, only result.

Following with the *MC* pattern, the *Model* is actually composed of independent models, one for each document (MongoDB's word for record, i.e entities/objects), and the same occurs with the *Controller* where many independent controllers take responsability of serving the control functions of each model.

Helper functions and objects are stored as *Utils* and preferably they only have one `module.export`;

As a contributor, your code should never break this architecture. If you mean to extend the functionalities of Mainframe, create an independent module under the corresponding section of the `src` folder and make sure that this module performs only one specific task, if your contribution reaches several tasks, create many independent modules for each task.

If your contribution also means adding a new API endpoint (e.g a new method to perform a general search), this should include, as minimum, one controller and one separate route, and preferably it's own model, even if it ever gets saved to the database.

As a side note to the microservices architecture, it is also worth mentioning that each module must be unit tested. Mainframe uses [Mocha](http://mochajs.org) with the [chai](http://chaijs.com) assertion library for testing. Preferably your code will also be developed by TDD, this means that you write the tests **before** you write the actual code.

### 12 factor app
12 factor is a developing philosophy which aims to erase or lesser the friction between the areas and phases of apps development. It is highly recommended that you read and follow [the 12 factors](https://www.12factor.net/).

As a TL:DR here is a summary:

1. **Codebase**
One codebase tracked in revision control, many deploys
2. **Dependencies**
Explicitly declare and isolate dependencies
3. **Config**
Store config in the environment
4. **Backing services**
Treat backing services as attached resources
5. **Build, release, run**
Strictly separate build and run stages
6. **Processes**
Execute the app as one or more stateless processes
7. **Port binding**
Export services via port binding
8. **Concurrency**
Scale out via the process model
9. **Disposability**
Maximize robustness with fast startup and graceful shutdown
10. **Dev/prod parity**
Keep development, staging, and production as similar as possible
11. **Logs**
Treat logs as event streams
12. **Admin processes**
Run admin/management tasks as one-off processes

Mainframe aims to be compliant with this philosophy.

## Contributing guidelines
Please take in consideration the following concerns before contributing:

1. App is is still in early development. Most features are already planned but not implemented yet.
2. Project is part of a bigger app, maybe the features you miss or the bug you found are not part of Mainframe

### Issues
When submiting an Issue, whether it is meta talk about Mainframe/Mercurio or a bug report, please be **respectful** to other people and their work. If you fail to agree to this term you will not be welcome as a contributor.

If you are submiting a bug, please provide as much info as you can; how did it happen, can you replicate it? what were you trying to accomplish? what is the outcome, what is the *expected* outcome?

### Commits
When submitting commits with new code please **make sure** that your code follows the [Design guidelines](#design-guidelines) detailed above. Code that does not will be rejected.

If your contribution is fairly large (several tens of lines of code), please split your contribution in multiple commits, each having different parts of the process. You should also use adecuate documentation and comments.


Thanks for your contribution to Mainframe.
