var Router = require('koa-router'),
    Validator = require('validatorjs'),
    router = new Router(),
    controller = require('../controllers/key');

/**
 * Respond to `POST` requests to `/key`
 * Generates a new Key document with given payload
 */
router.post('/key', async (ctx) => {
    let payload = ctx.request.body,
        rules = {
            appName: "required|string",
            appDescription: "required|string",
            appImg: "url"
        },
        validation = new Validator(payload, rules);
    
    if (validation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed payload',
                error: validation.errors.all()
            }
        }

        return;
    } 
    
    let post = await controller.post(payload);
    
    if (post.success){
        ctx.status = 201
        ctx.body = {
            status: 'success',
            data: {
                key: post.key
            }
        }
    }

    else {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Internal error',
                error: post.error
            }
        }
    }
});

/**
 * Respond to `PUT` requests to `/key/:id/:secret`
 * Updates an existing Key document
 */
router.put('/key/:id/:secret', async (ctx) => {
    let params = ctx.params,
        payload = ctx.request.body,
        paramsRules = {
            id: "required|digits:19",
            secret: "required|hex|size:40"
        },
        payloadRules = {
            appName: "string",
            appDescription: "string",
            appImg: "url"
        },
        paramsValidation = new Validator(params, paramsRules),
        payloadValidation = new Validator(payload, payloadRules);
    
    if (paramsValidation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed parameters',
                error: paramsValidation.errors.all()
            }
        }

        return;
    }

    if (payloadValidation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed payload',
                error: payloadValidation.errors.all()
            }
        }

        return;
    }
    
    let put = await controller.put(params.id, params.secret, payload);

    if (put.success) {
        ctx.status = 201
        ctx.body = {
            status: 'success',
            data: {
                key: put.key
            }
        }
    }

    else {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Internal error',
                error: put.error
            }
        }
    }
});

/**
 * Respond to `GET` requests to `/key/:id`
 * Returns a Key document without the secret, making Keys publicly available for consultation
 */
router.get('/key/:id', async (ctx) => {
    let params = ctx.params,
        rules = {
            id: "required|digits:19"
        },
        validation = new Validator(params, rules);

    if (validation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed parameters',
                error: validation.errors.all()
            }
        }

        return;
    }

    let get = await controller.getById(params.id);

    if (get.success) {
        ctx.status = 200
        ctx.body = {
            status: 'success',
            data: {
                key: get.key
            }
        }
    }

    else if (get.error === 'No results found') {
        ctx.status = 404
        ctx.body = {
            status: 'error',
            data: {
                message: 'Key could not be found',
                error: get.error
            }
        }
    }

    else {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Internal error',
                error: get.error
            }
        }
    }
});

/**
 * Respond to `GET` requests to `/key/:id/:secret`
 * Returns a key document with the secret, making the endpoint suitable tocheck secrets validity
 */
router.get('/key/:id/:secret', async (ctx) => {
    let params = ctx.params,
        rules = {
            id: "required|digits:19",
            secret: "required|hex|size:40"
        },
        validation = new Validator(params, rules);

    if (validation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed parameters',
                error: validation.errors.all()
            }
        }

        return;
    }

    let get = await controller.get(params.id, params.secret);

    if (get.success) {
        ctx.status = 200
        ctx.body = {
            status: 'success',
            data: {
                key: get.key
            }
        }
    }

    else if (get.error == 'No results found') {
        ctx.status = 404
        ctx.body = {
            status: 'error',
            data: {
                message: 'Key could not be found',
                error: get.error
            }
        }
    }

    else {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Internal error',
                error: get.error
            }
        }
    }
});

/**
 * Respond to `DELETE` requests to `/key/:id/:secret`
 * Deletes key documents from the database
 */
router.delete('/key/:id/:secret', async (ctx) => {
    let params = ctx.params,
        rules = {
            id: "required|digits:19",
            secret: "required|hex|size:40"
        },
        validation = new Validator(params, rules);

    if (validation.fails()) {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Invalid or malformed parameters',
                error: validation.errors.all()
            }
        }

        return;
    }

    let del = await controller.delete(params.id, params.secret);

    if (del.success) {
        ctx.status = 201
        ctx.body = {
            status: 'success',
            data: {
                message: 'Key deleted successfully'
            }
        }
    }

    else {
        ctx.status = 400
        ctx.body = {
            status: 'error',
            data: {
                message: 'Internal error',
                error: del.error
            }
        }
    }
});

module.exports = router;
