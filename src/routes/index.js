var Router = require('koa-router'),
    router = new Router();

/**
 * Respond to `GET` requests to `/`
 * i.e serves the landing page
 */
router.get('/', async (ctx) => {
    ctx.status = 200;
    ctx.type = 'text/html';
    ctx.body = `
        <title>Mainframe @ Mercurio</title>
        <p>
            <a 
                href="https://gitlab.com/wishiwasrubin/mercurio-mainframe"
                target="_blank"
                >Repository page</a>
        </p>
        <p>
            <a
                href="https://gitlab.com/wishiwasrubin/mercurio-mainframe-docs"
                target="_blank"
                >Documentation</a>
        </p>`;
});

module.exports = router;
