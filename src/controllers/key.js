"use strict";

var Key = require('../models/key');

/**
 * Find a key document by `_id`, won't select `secret`
 * @param {Number} id Value of `_id` property
 * @returns {Object} Promise
 */
module.exports.getById = function(id) {
    return Key
        .findById(id, [
            '_id', 
            'dateOfCreation', 
            'appName', 
            'appDescription', 
            'appImg'
        ]).exec()
        .then((key) => {
            if (key) {
                return {
                    success: true,
                    key: key
                }
            }

            return {
                error: 'No results found' 
            }
        })
        .catch((err) => {
            return {
                error: err
            }
        });
}

/**
 * Find a key document 
 * @param {Number} id Value of `_id` property
 * @param {String} secret Value of `secret` property
 * @returns {Object} Promise
 */
module.exports.get = function(id, secret) {
    return Key
        .findOne({
            _id: id,
            secret: secret
        })
        .exec()
        .then((key) => {
            if (key) {
                return {
                    success: true,
                    key: key
                }
            }

            return {
                error: 'No results found' 
            }
        })
        .catch((err) => {
            return {
                error: err
            }
        });
}

/**
 * Creates a key document with given parameters
 * @param {Object} data Object containing key document data
 * @returns {Object} Promise
 */
module.exports.post = function(data) {
    let key = new Key(data);

    return key
        .save()
        .then((key) => {
            return {
                success: true,
                key: key
            }
        })
        .catch((err) => {
            return {
                error: err
            }
        });
}

/**
 * Updates an existing key document
 * @param {Number} id Value of `_id` property
 * @param {Secret} secret Value of `secret` property
 * @param {Object} data Properties to be updated
 * @returns {Object} Promise
 */
module.exports.put = function(id, secret, data) {
    return Key
        .findOneAndUpdate({
            _id: id,
            secret: secret
        }, data, {useFindAndModify: false})
        .exec()
        .then((key) => {
            if (key) {
                return Key
                    .findById(key.id)
                    .exec()
                    .then((newKey) => {
                        return {
                            success: true,
                            key: newKey
                        }
                    });
            }

            return {
                error: 'Key does not exist'
            }
        })
        .catch((err) => {
            return {
                error: err
            }
        });
}

/**
 * Removes a key document
 * @param {Number} id Value of `_id` property
 * @param {String} secret Value of `secret` property
 * @returns {Object} Promise
 */
module.exports.delete = function(id, secret) {
    return Key
        .findOneAndRemove({
            _id: id,
            secret: secret
        }, {useFindAndModify: false})
        .exec()
        .then((key) => {
            return {
                success: true,
                key: key
            }
        })
        .catch((err) => {
            return {
                error: err
            }
        });
}
