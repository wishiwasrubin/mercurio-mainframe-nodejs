"use strict";

var mongoose = require('mongoose'),
    {simpleflake} = require('simpleflakes'),
    sha1 = require('sha1'),
    Schema = mongoose.Schema;

var keySchema = new Schema({
    _id: {
        type: Number,
        required: true,
        default: simpleflake().toString(),
        immutable: true
    },

    dateOfCreation: {
        type: Date,
        default: Date.now(),
        immutable: true
    },

    appName: {
        type: String,
        required: true
    },

    appDescription: {
        type: String,
        required: true
    },

    appImg: String,

    secret: {
        type: String,
        required: true,
        default: sha1(new Date(new Date().getTime() - Math.floor(Math.random() * 99999999999))),
        immutable: true
    }

});

module.exports = mongoose.model('Key', keySchema);
