"use strict";

var mongoose = require('mongoose'),
    {simpleflake} = require('simpleflakes'),
    Schema = mongoose.Schema;

var bookSchema = new Schema({
    _id: {
        type: Number,
        required: true,
        default: simpleflake().toString(),
        immutable: true
    },

    dateOfCreation: {
        type: Date,
        default: Date.now(),
        immutable: true
    },

    // Books can be self contained using this property to point to the _id of another book
    // this allows to nest and link books, i.e use chapters
    childOf: Number,
    
    updates: Array,

    title: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 66
    },

    body: String,

    coverUrl: {
        small: String,
        medium: String,
        large: String
    },

    author: {
        type: Number,
        required: true
    },

    tags: Array,

    meta: Array,

    ratings: {
        positive: Number,
        negative: Number,
        total: Number
    },

    comments: Array

});

module.exports = mongoose.model('Book', bookSchema);
