"use strict";

var mongoose = require('mongoose'),
    {simpleflake} = require('simpleflakes'),
    Schema = mongoose.Schema;

var userSchema = new Schema({
    _id: {
        type: Number,
        required: true,
        default: simpleflake().toString(),
        immutable: true
    },

    dateOfRegister: {
        type: Date,
        default: Date.now(),
        immutable: true
    },

    publicName: String,
    
    userName: {
        type: String,
        required: true
    },

    userImgUrl: {
        small: String,
        medium: String,
        large: String,
    },

    badges: Array,

    meta: Array,

    email: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true,
        minlength: 8
    },

    keys: Array

});

module.exports = mongoose.model('User', userSchema);
