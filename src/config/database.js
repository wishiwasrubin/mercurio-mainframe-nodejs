"use strict";

var dotenv = require('dotenv');
dotenv.config();

if (process.env.DB_USER !== undefined && process.env.DB_USER !== "") {
    var closedConn = `mongodb://` +
        `${process.env.DB_USER}:${process.env.DB_PASS}` +
        `@${process.env.DB_HOST}:${process.env.DB_PORT}` +
        `/${process.env.DB_OPTS ? process.env.DB_NAME + '?' + process.env.DB_OPTS : process.env.DB_NAME}`;

}

else {
    var closedConn = false;
    var openConn = `mongodb://` +
        `${process.env.DB_HOST}:${process.env.DB_PORT}` +
        `/${process.env.DB_OPTS ? process.env.DB_NAME + '?' + process.env.DB_OPTS : process.env.DB_NAME}`;
}

/**
 * Database location as an URL string
 * @returns {String} URL
 */
module.exports.location = closedConn ? closedConn.trim() : openConn.trim();

/**
 * Mongoose common `connect()` options
 * @returns {Object} Common options object
 */
module.exports.commOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}
