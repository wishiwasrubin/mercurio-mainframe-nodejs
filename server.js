var Koa = require('koa'),
    bodyParser = require('koa-bodyparser'),
    favicon = require('koa-favicon'),
    mongoose = require('mongoose'),
    indexRoute = require('./src/routes/index'),
    keyRoutes = require('./src/routes/key'),
    database = require('./src/config/database'),
    app = new Koa(),
    PORT = process.env.PORT || 3000;

app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(bodyParser());

mongoose.connect(database.location, database.commOptions);

app.use(indexRoute.routes());
app.use(keyRoutes.routes());

const server = app.listen(PORT, () => {
    console.log('Mainframe is up and running!');
    console.log(`Web Server at port: ${PORT}`);
    console.log(`Database at ${database.location}`);
});

module.exports = server;
