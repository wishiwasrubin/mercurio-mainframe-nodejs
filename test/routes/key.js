var chai = require('chai'),
    assert = chai.assert,
    chaiHttp = require('chai-http'),
    testKey = {},
    server = require('../../server');

chai.use(chaiHttp);

describe('Route: key', () => {

    describe('POST /key', () => {
        it('should create a new key and return document', (done) => {
            chai.request(server)
                .post('/key')
                .send({
                    appName: 'Test POST appName',
                    appDescription: 'Test POST appDescription'
                })
                .end((err, res) => {
                    assert.equal(res.status, 201);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'success');
                    assert.equal(res.body.data.key.appName, 'Test POST appName');
                    testKey = res.body.data.key;
                    done();
                });
        });

        it('should return error on invalid payload', (done) => {
            chai.request(server)
                .post('/key')
                .send({
                    appName: { notA: 'string' },
                    appDescription: 'string',
                })
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Invalid or malformed payload');
                    done();
                });
        });
    });

    describe(`PUT /key/:id/:secret`, () => {
        it('should update key and return document', (done) => {
            chai.request(server)
                .put(`/key/${testKey._id}/${testKey.secret}`)
                .send({
                    appName: 'Test Route Update',
                    appDescription: 'Test Route Update'
                })
                .end((err, res) => {
                    assert.equal(res.status, 201);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'success');
                    assert.equal(res.body.data.key.appName, 'Test Route Update');
                    done();
                });
        });

        it('should return error on invalid payload', (done) => {
            chai.request(server)
                .put(`/key/${testKey._id}/${testKey.secret}`)
                .send({
                    appName: { notA: 'string' },
                    appDescription: 'string',
                    secret: 'this new secret will be ignored, just like she ignores me :('
                })
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Invalid or malformed payload');
                    done();
                });
        });

        it('should return error on non existant parameters', (done) => {
            chai.request(server)
                .put(`/key/01234567890123456789/abcdef0123456789abcedf0123456789abcdef01`)
                .send({
                    appName: 'a valid payload',
                    appDescription: 'however it will get an error since key does not exist'
                })
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Invalid or malformed parameters');
                    done();
                });
        });
    });

    describe(`GET /key/:id`, () => {
        it('should return a key document without its secret', (done) => {
            chai.request(server)
                .get(`/key/${testKey._id}`)
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'success');
                    assert.equal(res.body.data.key.appName, 'Test Route Update');
                    assert.isNotOk(res.body.data.key.secret);
                    done();
                });
        });

        it('should return 404 on key not found', (done) => {
            chai.request(server)
                .get(`/key/0123456789012345678`)
                .end((err, res) => {
                    assert.equal(res.status, 404);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Key could not be found');
                    done();
                });
        });

        it('should return error on invalid parameter', (done) => {
            chai.request(server)
                .get('/key/invalidId')
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Invalid or malformed parameters');
                    done();
                });
        });
    });

    describe(`GET /key/:id/:secret`, () => {
        it('should return a key document', (done) => {
            chai.request(server)
                .get(`/key/${testKey._id}/${testKey.secret}`)
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'success');
                    assert.equal(res.body.data.key.appName, 'Test Route Update');
                    assert.equal(res.body.data.key.secret, testKey.secret);
                    done();
                });
        });

        it('should return 404 on key not found', (done) => {
            chai.request(server)
                .get(`/key/0123456789012345678/abcdef0123456789abcedf0123456789abcdef01`)
                .end((err, res) => {
                    assert.equal(res.status, 404);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Key could not be found');
                    done();
                });
        });

        it('should return error on invalid parameter', (done) => {
            chai.request(server)
                .get('/key/invalidId/invalidSecret')
                .end((err, res) => {
                    assert.equal(res.status, 400);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'error');
                    assert.equal(res.body.data.message, 'Invalid or malformed parameters');
                    done();
                });
        });
    });

    describe(`DELETE /key/:id/:secret`, () => {
        it('should delete key and return message', (done) => {
            chai.request(server)
                .delete(`/key/${testKey._id}/${testKey.secret}`)
                .end((err, res) => {
                    assert.equal(res.status, 201);
                    assert.equal(res.type, 'application/json');
                    assert.equal(res.body.status, 'success');
                    assert.equal(res.body.data.message, 'Key deleted successfully');
                    done();
                });
        });
    });

});
