var chai = require('chai'),
    chaiHttp = require('chai-http'),
    assert = chai.assert,
    server = require('../../server');

chai.use(chaiHttp);

describe('Route: index', () => {

    describe('GET /', () => {
        it('should return html', (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.type, 'text/html');
                    done();
                });
        });
    });

});
