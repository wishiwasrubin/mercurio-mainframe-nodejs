var chai = require('chai'),
    assert = chai.assert,
    expect = chai.expect,
    testKey = {},
    controller = require('../../src/controllers/key');

describe('Controller: key', () => {

    describe('post', () => {
        it('should generate a key', async () => {
            var data = {
                appName: 'Mocha Test Suite',
                appDescription: 'Mocha Test Suite',
            };
            var post = await controller.post(data);

            expect(post.success).to.exist;
            assert.equal(post.key.appName, 'Mocha Test Suite');
            testKey = post.key;
        });

        it('should throw an error if the payload is invalid', async () => {
            var data = {
                _id: 01234567,
            };
            var post = await controller.post(data);

            expect(post.error).to.exist;
        });
    });

    describe('put', () => {
        it('should update a key app', async () => {
            var data = {
                appName: 'Test Name',
                appDescription: 'Test Description',
            };
            var put = await controller.put(testKey._id, testKey.secret, data);

            expect(put.success).to.exist;
            assert.equal(put.key.appName, 'Test Name');
        });

        it('should throw an error if the payload is invalid', async () => {
            var data = {
                appName: 01234567,
                appDescription: new Array(),
                secret: 'this new secret will be ignored'
            };
            var put = await controller.put(testKey._id, testKey.secret, data);

            expect(put.error).to.exist;
        });
    });

    describe('getById', () => {
        it('should return a key without it secret', async () => {
            var get = await controller.getById(testKey._id);

            expect(get.success).to.exist;
            expect(get.key.secret).to.not.exist;
            assert.equal(get.key.appName, 'Test Name');
        });
    });

    describe('get', () => {
        it('should return a key with secret', async () => {
            var get = await controller.get(testKey._id, testKey.secret);

            expect(get.success).to.exist;
            expect(get.key.secret).to.exist;
            assert.equal(get.key.appName, 'Test Name');
        });

        it('should throw an error if key not found', async () => {
            var get = await controller.get(' ');

            expect(get.error).to.exist;
        });
    });

    describe('delete', () => {
        it('should delete a key by id', async () => {
            var del = await controller.delete(testKey._id, testKey.secret);
            var get = await controller.get(testKey._id, testKey.secret);

            expect(del.success).to.exist;
            expect(get.error).to.exist;
        });
    });

});
