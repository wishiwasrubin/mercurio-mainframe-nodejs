var chai = require('chai'),
    assert = chai.assert,
    expect = chai.expect,
    Model = require('../../src/models/key');

describe('Model: key', () => {

    it('should have snowflake _id', (done) => {
        var key = new Model();

        assert.isNumber(key._id);
        assert.equal(key._id.toString(2).length, 63);
        done();
    });

    it('should have sha1 secret', (done) => {
        var key = new Model();

        assert.isString(key.secret);
        expect(key.secret).to.match(/^[0-9a-f]*$/);
        assert.equal(key.secret.length, 40);
        done();
    });

});
