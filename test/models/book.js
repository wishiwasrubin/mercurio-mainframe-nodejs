var chai = require('chai'),
    assert = chai.assert,
    Model = require('../../src/models/book');

describe('Model: book', () => {

    it('should have snowflake _id', (done) => {
        var book = new Model();

        assert.isNumber(book._id);
        assert.equal(book._id.toString(2).length, 63);
        done();
    });

});
