var chai = require('chai'),
    assert = chai.assert,
    Model = require('../../src/models/user');

describe('Model: user', () => {

    it('should have snowflake _id', (done) => {
        var user = new Model();

        assert.isNumber(user._id);
        assert.equal(user._id.toString(2).length, 63);
        done();
    });

});
