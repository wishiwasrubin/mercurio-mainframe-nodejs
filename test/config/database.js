var chai = require('chai'),
    assert = chai.assert,
    expect = chai.expect,
    database = require('../../src/config/database');

describe('Config: database', () => {

    describe('location', () => {
        it('should return a mongodb URL', (done) => {
            expect(database.location).to.match(/(mongodb:\/\/)(.*)(\/)(.*)/);
            done();
        });
    });

    describe('commOptions', () => {
        it('should return object with common mongoose options', (done) => {
            assert.hasAllKeys(database.commOptions, ['useNewUrlParser', 'useUnifiedTopology']);
            done();
        });
    });

});
