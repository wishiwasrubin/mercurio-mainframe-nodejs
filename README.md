# Mercurio Mainframe
Courier. Not CMS.

This is the backing app that supports the Mercurio stack: FJE's spiritual successor.

### Objective of this app is
To be a RESTful CRUD API app with the following characteristics, in order of precendence:

1. **Microserviced**; easily scalable, maintenable and understandable. Unit tested.
2. **Long term support**. Built with latest but stablished, reliable, stable technologies.
3. Industry **standards compliant**: 12 factor app.

### Objectives of this app aren't
To be a full, frontend ready app. Models and controllers only. View layer built separately.

To be end-user ready. Common users friendliness is not required. Developers must consume this app and it's API from their own apps.

## Thanks to
This app is possible thanks to and works on top of the work of giants who freely and selflessly contributed to the following open source projects, thanks to you all:

[Node.js](https://nodejs.org) Server-side JavaScript execution

[npm](https://npmjs.com) Package manager for Node.js

[Koa](https://koajs.com/) Expressive, minimalist Node.js framework

[MongoDB](https://www.mongodb.com) Database 

[mongoose](https://mongoosejs.com) Object modeling for MongoDB 

[Mocha](https://mochajs.org) Easy & intuitive unit testing

[Chai](https://chaijs.com) Assertion library

[nodemon](https://nodemon.io) Node.js monitor

[skaterdav85/validatorjs](https://github.com/skaterdav85/validatorjs) Data validation library

[motdotla/dotenv](https://github.com/motdotla/dotenv) Load environment variables for Node.js

[leodutra/simpleflakes](https://github.com/leodutra/simpleflakes) id generation (inspired by Twitter's original snowflakes)

[pvorb/node-sha1](https://github.com/pvorb/node-sha1) SHA1 implementation for Node.js
